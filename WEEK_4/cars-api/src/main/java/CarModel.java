import java.util.ArrayList;
import java.util.List;

public class CarModel {
    private String model;
    private String codeModel;
    private CarMark markModel;


    public void setMarkModel(CarMark markModel){ this.markModel = markModel; }

    public CarMark getMarkModel(){return markModel;}

    public String getCodeModel(){
        return codeModel;
    }

    public void setCodeModel(String codeModel){
        this.codeModel =codeModel;
    }

    public String getModel(){
        return model;
    }

    public void setModel(String model){
        this.model = model;
    }
}
