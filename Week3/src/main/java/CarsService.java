import java.io.FileNotFoundException;

public class CarsService {
    private static CarsService service;

    private CarsService(){}

    public static CarsService getService(){
        if (service == null){
            service = new CarsService();
        }
        return service;
    }

    //Поиск машины по коду
    public Car searchCode(String code){
        Car[] mass= CarsData.getCars().getCarMass();
        Car request = new Car();
        boolean flag = false;
        for (Car i : mass){
            if(i.getCode().equals(code.trim())){
                request = i;
            }
        }
        return request;

    }


    //Поиск машины по марке
    public Car[] search(String mark){
        Car[] request = new Car[0];
        Car[] mass = CarsData.getCars().getCarMass();
        for (Car i : mass){
            if(i.getMark().getMark().equalsIgnoreCase(mark.trim())){
                request = CarsData.getCars().addCar(i,request);
            }
        }
        return request;
    }

    //Поиск машины по модели
    public Car[] search(String mark, String model){
        Car[] request = new Car[0];
        Car[] mass = CarsData.getCars().getCarMass();
        for (Car i : mass){
            if(i.getMark().getMark().equalsIgnoreCase(mark.trim()) &&
                    i.getModel().getModel().equalsIgnoreCase(model.trim())){
                request = CarsData.getCars().addCar(i,request);
            }
        }
        return request;
    }

    //Поиск машины по модели и цвету
    public Car[] search(String mark, String model, String color){
        Car[] request = new Car[0];
        Car[] mass = CarsData.getCars().getCarMass();
        for (Car i : mass){
            if(i.getMark().getMark().equalsIgnoreCase(mark.trim()) &&
                    i.getModel().getModel().equalsIgnoreCase(model.trim()) &&
                    i.getColor().toString().equalsIgnoreCase(color)){
                request = CarsData.getCars().addCar(i,request);
            }
        }
        return request;
    }

    //Поиск машины по году выпуска
    public Car[] search(int year){
        Car[] request = new Car[0];
        Car[] mass = CarsData.getCars().getCarMass();
        for (Car i : mass){
            if(i.getYear()== year){
                request = CarsData.getCars().addCar(i,request);
            }
        }
        return request;
    }

    //Поиск машины по году выпуска
    public Car[] search(double price){
        Car[] request = new Car[0];
        Car[] mass = CarsData.getCars().getCarMass();
        for (Car i : mass){
            if(i.getYear()== price){
                request = CarsData.getCars().addCar(i,request);
            }
        }
        return request;
    }




    //Сортировка машин по цене
    public Car[] sortPrice (){
        Car[] mass= CarsData.getCars().getCarMass();
        for (int i = 0; i < mass.length; i++)
        {
            Car min = mass[i];
            int min_i = i;
            for(int j = i + 1;j < mass.length; j++ ){
                if (mass[j].getPrice() < min.getPrice()){
                    min = mass[j];
                    min_i = j;
                }
            }
            if (i != min_i){
                Car buf = mass[i];
                mass[i] = mass[min_i];
                mass[min_i] = buf;
            }
        }
        for (Car i : mass){
            System.out.println("Код машины " +i.getCode() +  " её цена: " + i.getPrice());
        }
        return mass;
    }
}
