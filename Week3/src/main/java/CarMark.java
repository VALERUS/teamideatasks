public class CarMark {
    private String mark;
    private String codeMark;

    public String getCodeMark(){
        return codeMark;
    }

    public void setCodeMark(String codeMark){
        this.codeMark =codeMark;
    }


    public String getMark(){
        return mark;
    }

    public void setMark(String mark){
        this.mark = mark;
    }
}
