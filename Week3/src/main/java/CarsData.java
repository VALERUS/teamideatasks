import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Scanner;


public class CarsData {
    private static CarsData cars;
    private Car[] carMass;
    private CarsData() {
        carMass = new Car[0];
        try {
            fillData();
        }catch (FileNotFoundException e){
            System.out.println("Файл не найден");
        }

    }

    public static CarsData getCars(){
        if (cars == null){
            cars = new CarsData();
        }
        return cars;
    }

    public Car[] getCarMass(){
        return carMass.clone();
    }

    public Car[] addCar(Car car, Car[]carMass) {
        Car[] newArray = new Car[carMass.length+1];
        int i = carMass.length;
        System.arraycopy(carMass, 0, newArray, 0, i);
        carMass = newArray;
        carMass[i] = car;
        return carMass;
    }


    private void fillData ()throws FileNotFoundException {
        FileReader fr = new FileReader("cars.dat");;
        Scanner read = new Scanner(fr);

        while (read.hasNextLine()) {
            Car car = new Car();
            String[] line = read.nextLine().split("\\;");
            if (line.length == 0 || line.length == 1) {
                continue;
            }
            int k;
            if (line[0].equals("")) {
                k = 1;
            } else {
                k = 0;
            }

            //заполнение поля code
            if (line[k].equals("")) {
                continue;
            } else {
                car.setCode(line[k].trim());
                k++;
            }

            //Заполнение поля color
            if (line.length == k + 1) {
                continue;
            } else {
                try {
                    boolean flag = false;
                    for (CarColor color : CarColor.values()) {
                        if (color.toString().equalsIgnoreCase(line[k].replaceAll(" ", ""))) {
                            car.setColor(color);
                            k++;
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        throw new IllegalArgumentException("Неверно введён цвет");
                    }
                }catch (IllegalArgumentException e) {
                    continue;
                }
                //Заполнение поля mark

                if (line.length == k + 1) {
                    continue;
                } else {
                    if (line[k].trim().equals("")) {
                        continue;
                    } else {
                        car.getMark().setMark(line[k].trim());
                        k++;
                    }
                }

                //Заполнение поля model

                if (line.length == k + 1) {
                    continue;
                } else {
                    if (line[k].trim().equals("")) {
                        continue;
                    } else {
                        car.getModel().setModel(line[k].trim());
                        k++;
                    }
                }
                //Заполнение поля year
                if (line.length == k + 1) {
                    continue;
                } else {
                    try {
                        if (line[k].trim().replaceAll("[^\\d]", "").equals("")) {
                            throw new NumberFormatException("Неверно введён год выпуска");
                        }
                    } catch (NumberFormatException e) {
                        continue;
                    }
                    car.setYear(Integer.parseInt(line[k].trim().replaceAll("[^\\d]", "")));
                    k++;
                }
                //Заполнение поля price
                try {
                    if (line[k].trim().replaceAll("[^\\d\\.]", "").charAt(0) == '.') {
                        throw new NumberFormatException("Неверно введена цена");
                    } else {
                        car.setPrice(Double.parseDouble(line[k].replaceAll("[^\\d\\.]", "")));
                    }
                } catch (NumberFormatException e) {
                    continue;
                }
                carMass = addCar(car, carMass);
            }
        }

    }
}
