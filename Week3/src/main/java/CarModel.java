public class CarModel {
    private String model;
    private String codeModel;

    public String getCodeModel(){
        return codeModel;
    }

    public void setCodeModel(String codeModel){
        this.codeModel =codeModel;
    }

    public String getModel(){
        return model;
    }

    public void setModel(String model){
        this.model = model;
    }
}
