public class Car {
    private String code;
    private int year;
    private double price;
    private CarColor color;
    private CarMark mark;
    private CarModel model;

    Car (){
        mark = new CarMark();
        model = new CarModel();
        code = "10";
        year = 0;
        price = 0.0;
    }

    public String getCode(){
        return code;
    }
    public int getYear(){
        return year;
    }
    public double getPrice(){
        return price;
    }
    public CarColor getColor (){
        return color;
    }
    public CarMark getMark(){
        return mark;
    }
    public CarModel getModel(){
        return model;
    }


    public void setCode (String code){
        this.code = code;
    }
    public void setYear (int year){
        this.year = year;
    }
    public void setPrice (double price){
        this.price = price;
    }
    public void  setColor (CarColor color){
        this.color = color;
    }
    public void setMark (CarMark mark){
        this.mark = mark;
    }
    public void setModel(CarModel model){
        this.model = model;
    }


}
