import java.util.ArrayList;
import java.util.List;

public class CarMark {
    private String mark;
    private String codeMark;
    private List<CarModel> models = new ArrayList<CarModel>();

    public void setModels(List models){this.models = models;}

    public List getModels(){ return models; }
    public CarModel[] getModelsMass(){ return models.toArray(new CarModel[models.size()].clone()); }

    public String getCodeMark(){
        return codeMark;
    }

    public void setCodeMark(String codeMark){
        this.codeMark =codeMark;
    }


    public String getMark(){ return mark; }

    public void setMark(String mark){
        this.mark = mark;
    }
}
