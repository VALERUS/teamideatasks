
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class CarsServiceImpl implements CarsService{

    private static CarsServiceImpl service;


    private CarsServiceImpl(){}

    public static CarsServiceImpl getService(){
        if (service == null){
            service = new CarsServiceImpl();
        }
        return service;
    }


    public void searchCars(){
        Scanner sc = new Scanner(System.in);
        List<String> line;
        do{
            System.out.print("Поиск: ");
            String[] lineBuf = sc.nextLine().split(" ");
            line = new ArrayList<String>();
            for (String i: lineBuf){
                if (!i.equals("")){
                    line.add(i);
                }
            }
            if (line.size()== 0){
                break;
            }

            if (line.get(0).equals("1".trim())){
                showResult(CarsDataImpl.getCars().getCarMass());
                continue;
            } else{
                if(line.get(0).equals("2".trim())){
                   showName(CarsDataImpl.getCars().getCarMass());
                   continue;
                }

            }
            switch (line.size()){
                case 1:{
                    try{
                        if (sortPrice(search(Integer.parseInt(line.get(0)))).length != 0){
                            showResult(sortPrice(search(Integer.parseInt(line.get(0)))));
                            break;
                        }else{
                            if (!searchCode(line.get(0)).getCode().equals("")){
                                showResult(searchCode(line.get(0)));
                                break;
                            }else{
                                if(search(Double.parseDouble(line.get(0))).length != 0){
                                    showResult(search(Double.parseDouble(line.get(0))));
                                    break;
                                }
                                System.out.println("Извините, но ничего не найдено по данному запросу.\n" +
                                        "Для выхода нажмите enter, если хотите просмотреть весь список автомобилей нажмите 1, " +
                                        " если хотите просмотреть весь список доступных " +
                                        "Марок автомобилей нажмите 2, или продолжите поиск. ");
                                break;
                            }

                        }

                    }catch (NumberFormatException e){
                        try{
                            showResult(search(Double.parseDouble(line.get(0))));
                            break;
                        }catch (NumberFormatException ex){
                            if(search(line.get(0)).length != 0){
                                Car[] res = search(line.get(0));
                                showResult(search(line.get(0)));
                                System.out.println("Если хотите получить список моделей данной марки нажмите 1");
                                String on = sc.nextLine();
                                if (on.trim().equals("1")){
                                    showName(search(line.get(0))[0].getMark().getModelsMass());
                                }
                                break;
                            }else{
                                if (searchAdd(line.get(0)).length != 0){
                                    showResult(searchAdd(line.get(0)));
                                    break;
                                }else{
                                    if (searchAdd2(line.get(0)).length !=0){
                                        showResult(searchAdd2(line.get(0)));
                                        break;
                                    }
                                }
                            }
                            System.out.println("Извините, но ничего не найдено по данному запросу.\n" +
                                    "Для выхода нажмите enter, если хотите просмотреть весь список автомобилей нажмите 1," +
                                    " " + " если хотите просмотреть весь список доступных " +
                                    "Марок автомобилей нажмите 2, или продолжите поиск. ");
                            break;
                        }
                    }
                }
                case 2:{
                    try {
                        if (search(line.get(0),Integer.parseInt(line.get(1))).length != 0 ){
                            showResult(sortPrice(search(line.get(0),Integer.parseInt(line.get(1)))));
                            break;
                        }else{
                            if (searchAdd(line.get(0),Integer.parseInt(line.get(1))).length != 0){
                                showResult(sortPrice(searchAdd(line.get(0),Integer.parseInt(line.get(1)))));
                                break;
                            }
                        }

                    }catch (NumberFormatException e){
                        try{
                            if (search(line.get(0),Double.parseDouble(line.get(1))).length != 0){
                                showResult(search(line.get(0),Double.parseDouble(line.get(1))));
                                break;
                            }else{
                                if (searchAdd(line.get(0),Double.parseDouble(line.get(1))).length != 0){
                                    showResult(searchAdd(line.get(0),Double.parseDouble(line.get(1))));
                                    break;
                                }
                            }
                        }catch (NumberFormatException ex){
                            if (search(line.get(0),line.get(1)).length != 0){
                                showResult(search(line.get(0),line.get(1)));
                                break;
                            }else{
                                if (searchAdd(line.get(0),line.get(1)).length != 0){
                                    showResult(searchAdd(line.get(0),line.get(1)));
                                    break;
                                }else{
                                    if (searchAdd2(line.get(0),line.get(1)).length != 0){
                                        showResult(searchAdd2(line.get(0),line.get(1)));
                                        break;
                                    }
                                }
                            }
                            System.out.println("Извините, но ничего не найдено по данному запросу.\n" +
                                    "Для выхода нажмите enter, если хотите просмотреть весь список автомобилей нажмите 1," +
                                    " " + " если хотите просмотреть весь список доступных " +
                                    "Марок автомобилей нажмите 2, или продолжите поиск. ");
                            break;
                        }
                    }

                }
                default:{
                    System.out.println("Вы ввели неверное значание для поиска");
                    break;
                }
            }
        }while(line.size() != 0);



    }

    //Поиск машины по коду
    private Car searchCode(String code){
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        Car request = new Car();
        for (Car i : mass){
            if(i.getCode().equals(code.trim())){
                request = i;
                break;
            }
        }
        return request;

    }


    //Поиск машины по марке
    private Car[] search(String mark){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getMark().getMark().equalsIgnoreCase(mark.trim())){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по марке и году выпуска
    private Car[] search(String mark, int year){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getMark().getMark().equalsIgnoreCase(mark.trim()) &&
                    i.getYear()== year){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по модели и году выпуска
    private Car[] searchAdd(String model, int year){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getModel().getModel().equalsIgnoreCase(model.trim()) &&
                    i.getYear()== year){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по модели и цене
    private Car[] searchAdd(String model, double price){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getModel().getModel().equalsIgnoreCase(model.trim()) &&
                    i.getPrice()== price){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по марке и цвету
    private Car[] searchAdd2(String mark, String color){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getMark().getMark().equalsIgnoreCase(mark.trim()) &&
                    i.getColor().toString().equalsIgnoreCase(color)){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по моделе и цвету
    private Car[] searchAdd(String model, String color){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getModel().getModel().equalsIgnoreCase(model.trim()) &&
                    i.getColor().toString().equalsIgnoreCase(color)){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по марке и цене
    private Car[] search(String mark, double price){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getMark().getMark().equalsIgnoreCase(mark.trim()) &&
                    i.getPrice()== price){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по модели
    private Car[] searchAdd(String model){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getModel().getModel().equalsIgnoreCase(model.trim())){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //поиск машины по марке и моделе
    private Car[] search(String mark, String model){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getMark().getMark().equalsIgnoreCase(mark.trim()) &&
                    i.getModel().getModel().equalsIgnoreCase(model.trim())){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по цвету
    private Car[] searchAdd2(String color){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getColor().toString().equalsIgnoreCase(color.trim())){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }


    //Поиск машины по году выпуска
    private Car[] search(int year){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getYear()== year){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Поиск машины по цене
    private Car[] search(double price){
        List<Car> request = new ArrayList<Car>();
        Car[] mass = CarsDataImpl.getCars().getCarMass();
        for (Car i : mass){
            if(i.getPrice()== price){
                request.add(i);
            }
        }
        return request.toArray(new Car[request.size()]);
    }

    //Сортировка машин по цене
    private Car[] sortPrice (Car[] mass){
        for (int i = 0; i < mass.length; i++)
        {
            Car min = mass[i];
            int min_i = i;
            for(int j = i + 1;j < mass.length; j++ ){
                if (mass[j].getPrice() < min.getPrice()){
                    min = mass[j];
                    min_i = j;
                }
            }
            if (i != min_i){
                Car buf = mass[i];
                mass[i] = mass[min_i];
                mass[min_i] = buf;
            }
        }
        return mass;
    }

    //Методы для вывода данных об автомобиле
    private void showResult (Car[] request){
        for (Car i : request){
            System.out.println(i.getMark().getMark() + " "
                    + i.getModel().getModel() + " "
                    + i.getColor() + " "
                    + i.getYear() + " "
                    + i.getPrice());
        }
        System.out.println("Для выхода нажмите enter, если хотите просмотреть весь список автомобилей нажмите 1, " +
                " если хотите просмотреть весь список доступных Марок автомобилей нажмите 2, или продолжите поиск. ");
    }

    private void showResult (Car request){
        System.out.println(request.getMark().getMark() + " "
                + request.getModel().getModel() + " "
                + request.getColor() + " "
                + request.getYear() + " "
                + request.getPrice());
        System.out.println("Для выхода нажмите enter, если хотите просмотреть весь список автомобилей нажмите 1, " +
                " если хотите просмотреть весь список доступных Марок автомобилей нажмите 2, или продолжите поиск");
    }

    //Методы для вывода всех имеющихся марок и моделей(без повторений)
    private void showName (Car[] cars){
        List<String> marks = new ArrayList<String>();
        for (Car i : cars){
            marks.add(i.getMark().getMark());
        }
        for (int i = 0; i < marks.size(); i++){
            for(int j = i + 1; j < marks.size(); j++){
                if (marks.get(i).equalsIgnoreCase(marks.get(j))){
                    marks.remove(j);
                }
            }
        }
        for (String i: marks){
            System.out.println(i);
        }
    }
    private void showName (CarModel[] cars){
        List<String> models = new ArrayList<String>();
        for (CarModel i:cars){
            models.add(i.getModel());
        }
        for (int i = 0; i < models.size(); i++){
            for (int j = i + 1; j < models.size(); j++){
                if(models.get(i).equalsIgnoreCase(models.get(j))){
                    models.remove(j);
                }
            }
        }
        for (String i : models){
            System.out.println(i);
        }

    }

}

