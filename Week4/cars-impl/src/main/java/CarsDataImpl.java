
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.util.ArrayList;

import java.util.Scanner;
import java.util.List;
import java.util.logging.Logger;


public class CarsDataImpl implements CarsData{

    private final static Logger log =  Logger.getLogger(CarsDataImpl.class.getName());
    private static CarsDataImpl cars;
    private List<Car> carMass;
    private CarsDataImpl() {
        carMass = new ArrayList<Car>();
        try{
            fillData();
        }catch (FileNotFoundException e){
            log.info(e.getMessage());
        }
    }

    public static CarsDataImpl getCars(){
        if (cars == null){
            cars = new CarsDataImpl();
        }
        return cars;
    }

    public Car[] getCarMass(){
        return carMass.toArray(new Car[carMass.size()].clone());
    }

    private void fillData ()throws FileNotFoundException{
        FileReader fr = new FileReader("cars.dat");
        Scanner sc = new Scanner(fr);
        while(sc.hasNextLine()){
            String[] line = sc.nextLine().split(";");
            if (line.length == countPropertiesCars || line.length == countPropertiesCars + 1){
                int k;
                Car car = new Car();
                if (line.length == countPropertiesCars){
                    k = 0;
                }else{
                    k = 1;
                }

                //Заполнение поля code и codeMark
                if (line[k].equals("")){
                    continue;
                }else{
                    car.setCode(line[k]);
                    k++;
                }

                //Заполнение поля color
                if (line[k].equals("")){
                    continue;
                }else{
                    try{
                        boolean flag = true;
                        for(CarColor color: CarColor.values()){
                            if(color.toString().equalsIgnoreCase(line[k].replaceAll(" ", ""))){
                                car.setColor(color);
                                flag = false;
                                k ++;
                                break;
                            }
                        }
                        if(flag){
                            throw new IllegalArgumentException();
                        }
                    }catch (IllegalArgumentException e){
                        continue;
                    }
                }


                //Заполнение объекта carMark
                if (line[k].equals("")){
                    continue;
                }else{
                    car.getMark().setMark(line[k].trim());
                    car.getMark().setCodeMark(car.getCode());
                    k ++;
                }
                //Заполнение объекта carMark
                if (line[k].equals("")){
                    continue;
                }else{
                    car.getModel().setCodeModel(car.getCode());
                    car.getModel().setModel(line[k].trim());
                    k++;
                }

                //Заполнение поля year
                if (line[k].equals("")){
                    continue;
                }else{
                    try {
                        if (line[k].trim().replaceAll("[^\\d]", "").equals("")) {
                            throw new NumberFormatException("Неверно введён год выпуска");
                        }
                    } catch (NumberFormatException e) {
                        continue;
                    }
                    car.setYear(Integer.parseInt(line[k].trim().replaceAll("[^\\d]", "")));
                    k++;
                }

                //Заполнение поля price
                if (line[k].equals("")){
                    continue;
                }else{
                    try {
                        if (line[k].trim().replaceAll("[^\\d\\.]", "").charAt(0) == '.') {
                            throw new NumberFormatException("Неверно введена цена");
                        } else {
                            car.setPrice(Double.parseDouble(line[k].replaceAll("[^\\d\\.]", "")));
                        }
                    } catch (NumberFormatException e) {
                        continue;
                    }
                }
                carMass.add(car);
            }else{
                continue;
            }
        }
        listModels(carMass);

    }

    private void listModels(List<Car> cars){
        for (int i = 0; i < cars.size(); i++){
            cars.get(i).getMark().getModels().add(cars.get(i).getModel());
            for (int j = i + 1; j < cars.size(); j++){
                if (cars.get(j).getMark().getMark().equalsIgnoreCase(cars.get(i).getMark().getMark())){
                    cars.get(i).getMark().getModels().add(cars.get(j).getModel());
                }
            }
            cars.get(i).getModel().setMarkModel(cars.get(i).getMark());
        }

    }
}
